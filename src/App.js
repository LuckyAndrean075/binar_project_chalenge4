import "./App.scss";
import "swiper/swiper.min.css";
import "./assets/boxicons-2.0.7/css/boxicons.min.css";

import {
  BrowserRouter,
  Outlet,
  Route,
  RouterProvider,
  Routes,
  createBrowserRouter,
} from "react-router-dom";

import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";

import Home from "./pages/Home";
import Catalog from "./pages/Catalog";
import Detail from "./pages/detail/Detail";

function App() {
  const Layout = () => {
    return (
      <>
        <Header />
        <Outlet />
        <Footer />
      </>
    );
  };

  const router = createBrowserRouter([
    {
      path: "/",
      element: <Layout />,
      children: [
        {
          path: "/",
          element: <Home />,
        },
        {
          path: "/:category",
          element: <Catalog />,
        },
        {
          path: "/:category/search/:keyword",
          element: <Catalog />,
        },
        {
          path: "/:category/:id",
          element: <Detail />,
        },
      ],
    },
  ]);
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default App;

// import "./App.scss";
// import "swiper/swiper.min.css";
// import "./assets/boxicons-2.0.7/css/boxicons.min.css";

// import { BrowserRouter, Routes, Route } from "react-router-dom";

// import Header from "./components/header/Header";
// import Footer from "./components/footer/Footer";
// import Home from "./pages/Home";
// import Catalog from "./pages/Catalog";
// import Detail from "./pages/Detail";

// import Routing from "./config/Routing";

// function App() {
//   return (
//     <BrowserRouter>
//       <Header />
//       <Routes>
//         <Route path="/" element={<Home />} />
//         <Route path="/movies" element={<Catalog />} />
//         <Route path="/movies/id" element={<Detail />} />
//       </Routes>
//       <Footer />
//     </BrowserRouter>
//   );
// }

// export default App;
